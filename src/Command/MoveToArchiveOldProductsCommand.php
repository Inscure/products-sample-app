<?php
declare(strict_types=1);

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MoveToArchiveOldProductsCommand extends Command
{
    protected static $defaultName = 'MoveToArchiveOldProducts';
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this->setDescription('Ustawia jako nieaktywne produkty starsze niż miesiąc.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        // 1. warto opakowac to w klase by ukryc ze dzialamy na enity managerze,
        // tym samym ukrywajac miejsce przechowywania danych (db), ale zwazywszy na limit czasu zadania nie robie tego;
        // 2. rozdzielilbym query (repo) od aktualizacji encji, wiec  nie wrzucalbym tego do klasy repo
        // tylko dodal zupelnie osobna klase (zgodnie z zasadą COMMAND-QUERY SEPARATION).

        $query = $this->entityManager->createQuery(
            'UPDATE App\Entity\Product p SET p.active = false WHERE p.createdAt < :monthBefore'
        );

        $query->execute(['monthBefore' => new \DateTime('-1 month')]);

        $io->success('Aktualizacja rekordów zakończona pomyślnie.');

        return Command::SUCCESS;
    }
}
