<?php
declare(strict_types=1);

namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class SendEmailService
{
    private MailerInterface $mailer;
    private string $emailRecipient;
    private string $emailSender;

    public function __construct(MailerInterface $mailer, string $emailRecipient, string $emailSender)
    {
        $this->mailer = $mailer;
        $this->emailRecipient = $emailRecipient;
        $this->emailSender = $emailSender;
    }

    public function sendEmail(string $content, string $name): void
    {
        $emailBody = $content . $name;

        $email = (new Email())
            ->from($this->emailSender)
            ->to($this->emailRecipient)
            ->subject('Jakis tytul...')
            ->text($emailBody) // tu ewentualne usuwanie html, zamiana br na php eol etc.
            ->html($emailBody);

        $this->mailer->send($email);
    }
}