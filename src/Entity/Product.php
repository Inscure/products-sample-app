<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private string $name;

    /**
     * @ORM\Column(type="decimal", precision=9, scale=2, nullable=false)
     */
    private string $price;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private ?string $channel;

    /**
     * @ORM\Column(type="boolean", options={"default": true}, nullable=false)
     */
    private bool $active;

    /**
     * @ORM\Column(type="datetimetz_immutable", nullable=false)
     */
    private \DateTimeInterface $createdAt;

    public function __construct(string $name, string $price, ?string $channel = null)
    {
        $this->setName($name);
        $this->setPrice($price);
        $this->setChannel($channel);

        $this->active = true;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): string
    {
        return $this->price;
    }

    public function getChannel(): ?string
    {
        return $this->channel;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setName(string $name): self
    {
        if (strlen($name) > 150) {
            throw new \UnexpectedValueException('Przekroczona maksymalna długość nazwy.');
        }

        $this->name = $name;
        return $this;
    }

    public function setPrice(string $price): self
    {
        if (!preg_match('/^(\d{1,7})$|^(\d{1,7}\.\d{1,2})$/', $price)) {
            throw new \UnexpectedValueException('Nieprawidłowy format kwoty.');
        }

        $this->price = $price;
        return $this;
    }

    public function setChannel(?string $channel): self
    {
        if ($channel !== null && strlen($channel) > 20) {
            throw new \UnexpectedValueException('Przekroczona maksymalna długość nazwy kanału sprzedaży.');
        }

        $this->channel = $channel;
        return $this;
    }
}
