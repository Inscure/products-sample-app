<?php
declare(strict_types=1);

namespace App;

class ProductDto
{
    public ?string $name = null;
    public ?string $price = null;
    public ?string $channel = null;
}