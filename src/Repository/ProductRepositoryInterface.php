<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Product;

interface ProductRepositoryInterface
{
    /**
     * @param int $limit
     * @return Product[]
     */
    public function findRecentlyAddedOnlyActive(int $limit): array;

    public function save(Product $product): void;

}