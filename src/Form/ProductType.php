<?php
declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Nazwa',
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Length(['max' => 150])
                ]
            ])
            ->add('price', null, [
                'label' => 'Cena',
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Regex([
                        'pattern' => '/^(\d{1,7})$|^(\d{1,7}\.\d{1,2})$/',
                    ])
                ]
            ])
            ->add('channel', null, [
                'label' => 'Kanał sprzedaży',
                'required' => false,
                'constraints' => [
                    new Length(['max' => 20])
                ]
            ])
        ;
    }
}
