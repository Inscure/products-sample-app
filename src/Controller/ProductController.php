<?php
declare(strict_types=1);

namespace App\Controller;

use App\Builder\ProductDtoBuilder;
use App\Builder\ProductBuilder;
use App\ProductDto;
use App\Entity\Product;
use App\ProductFiller;
use App\Form\ProductType;
use App\Repository\ProductRepositoryInterface;
use App\Service\SendEmailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ProductController extends AbstractController
{
    private const PRODUCTS_COUNT_PER_PAGE = 10; // mozna wyniesc do config.yml lub nawet .env

    private ProductRepositoryInterface $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index(SendEmailService $sendEmailService): Response
    {
        return $this->render('product/index.html.twig', [
            'products' => $this->productRepository->findRecentlyAddedOnlyActive(self::PRODUCTS_COUNT_PER_PAGE)
        ]);
    }

    public function new(ProductBuilder $productBuilder, Request $request): Response
    {
        $form = $this->createForm(ProductType::class, new ProductDto());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->productRepository->save(
                $productBuilder->build($form->getData())
            );

            return $this->redirectToRoute('product_index');
        }

        return $this->render('product/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function show(Product $product): Response
    {
        if (!$product->isActive()) {
            throw $this->createNotFoundException();
        }

        return $this->render('product/show.html.twig', [
            'product' => $product,
        ]);
    }

    public function edit(
        ProductDtoBuilder $productDtoBuilder,
        ProductFiller $productFiller,
        Request $request,
        Product $product
    ): Response {
        if (!$product->isActive()) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(ProductType::class, $productDtoBuilder->build($product));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $productFiller->fill($product, $form->getData());

            $this->productRepository->save($product);

            return $this->redirectToRoute('product_index');
        }

        return $this->render('product/edit.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }
}
