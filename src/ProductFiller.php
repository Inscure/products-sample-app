<?php
declare(strict_types=1);

namespace App;

use App\Entity\Product;

class ProductFiller
{
    public function fill(Product $product, ProductDto $productDto)
    {
        $product
            ->setName($productDto->name)
            ->setPrice($productDto->price)
            ->setChannel($productDto->channel);
    }
}