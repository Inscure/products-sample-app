<?php
declare(strict_types=1);

namespace App\Builder;

use App\ProductDto;
use App\Entity\Product;

class ProductBuilder
{
    public function build(ProductDto $productDto): Product
    {
        return new Product($productDto->name, $productDto->price, $productDto->channel);
    }
}