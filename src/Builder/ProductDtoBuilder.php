<?php
declare(strict_types=1);

namespace App\Builder;

use App\ProductDto;
use App\Entity\Product;

class ProductDtoBuilder
{
    public function build(Product $product): ProductDto
    {
        $productDto = new ProductDto;
        $productDto->name = $product->getName();
        $productDto->price = $product->getPrice();
        $productDto->channel = $product->getChannel();

        return $productDto;
    }
}