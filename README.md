## Opis projektu 

System do dodawania, edycji i przeglądu produktów. 
Umożliwia określenie ceny, kanału sprzedaży, nazwy produktu. 

Struktura modelu produktu:
1. Identyfikator produktu (wartość numeryczna, nadawana przez system)
2. Nazwa produktu (wartość tekstowa, obligatoryjna, podawana przez użytkownika, maksymalnie 150 znaków)
3. Cena produktu (wartość liczbowa, obligatoryjna, podawana przez użytkownika z dokładnością do jednego lub dwóch miejsc po kropce albo w formie liczby całkowitej)
4. Kanał sprzedaży (wartość tekstowa, opcjonalna, podawana przez użytkownika, maksymalnie 20 znaków)

Wszystkie produkty domyślnie są aktywne. 
Zmiana na nieaktywne odbywa się poprzez skrypt (zobacz sekcję "Dostępne komendy") dla produktów starszych niż miesiąc. 
Każdy dodany do systemu produkt uzyskuje automatycznie datę dodania.

----

## Uruchomienie 

Przed próbą uruchomienia projektu upewnij się, że posiadasz wolne porty 8094 oraz 3306. 
Do uruchomienia projektu potrzebujesz zainstalowanego Dockera oraz Docker Compose. 
Rozwiązanie testowane było na wersji 20.10.5.

Wykonaj jednorazowo poniższe polecenia (utworzone zostaną kontenery, zainstalowane zależności oraz wgrany zostanie schemat bazy danych) : 

```
docker-compose up --build
docker exec -it lemonmind_php bin/composer.phar install
docker exec -it lemonmind_php bin/console d:m:m
```

System dostępny będzie pod adresem http://localhost:8094/

Jeśli chcesz skorzystać z serwisu `App\Service\SendEmailService`, uzupełnij prawidłowymi danymi stałą środowiskową 
MAILER_DSN w pliku docker-compose.yml i przebuduj kontenery poleceniem `docker-compose up --build`.

----

## Dostępne komendy:

Aktualizacja produktów starszych niż miesiąc (oznaczenie jako nieaktywne):

```
docker exec -it lemonmind_php bin/console MoveToArchiveOldProducts
```

## Uruchomienie testów phpunit

W celu uruchomienia testów należy wykonać poniższe polecenia:

```
docker exec -it lemonmind_php bash
cd bin
./phpunit -c ../phpunit.xml.dist
```

Powyższe trzy polecenia w teorii można skrócić, ale phpunit wymaga by uruchamiać go z katalogu bin, inaczej nie znajdzie composera, który jest dla niego niezbędny by pobrac zależności. 
