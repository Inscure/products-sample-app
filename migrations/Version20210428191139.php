<?php
declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210428191139 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(150) NOT NULL, price NUMERIC(9, 2) NOT NULL, channel VARCHAR(20) DEFAULT NULL, active TINYINT(1) DEFAULT \'1\' NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetimetz_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('INSERT INTO product (name, price, channel, active, created_at) VALUES (\'testowa\', 55.66, \'test\', 0, \'2021-02-28 23:23:18\');');
        $this->addSql('INSERT INTO product (name, price, channel, active, created_at) VALUES (\'testowa\', 55.66, \'test\', 1, \'2021-02-28 23:23:18\');');
        $this->addSql('DELETE FROM product WHERE id = 1');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE product');
    }
}
