<?php
declare(strict_types=1);

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductTest extends WebTestCase
{
    public function testExists(): void
    {
        $client = static::createClient();
        $client->request('GET', '/product/2'); // bazuje tu na dodanych w migracji rekordach do bazy danych
                                               // ale tak docelowo to startujac testy powinien wgrac sie zestaw danych

        $this->assertResponseStatusCodeSame(200);
    }

    public function testNotExists(): void
    {
        $client = static::createClient();
        $client->request('GET', '/product/1'); // bazuje tu na dodanych w migracji rekordach do bazy danych
                                               // ale tak docelowo to startujac testy powinien wgrac sie zestaw danych

        $this->assertResponseStatusCodeSame(404);
    }
}
